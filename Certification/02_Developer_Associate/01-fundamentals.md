# AWS Fundamentals

## IAM & EC2

### Regions and AZs

* Region is cluster of data centers
* AZ - one or more discrete data centers 

A Region comprises of 2 to 6 AZs (usually 3).  

**Regional table** shows which AWS services are offered in which region.

### IAM

IAM handles AWS security and consists of Users, Groups, Roles 
and Policies to attach to them.

**IAM federation** - Uses the SAML standard and is used to have own repository of
users integrated with IAM (so you can login in AWS with company credentials)

### EC2

The EC2 service offers:

* Renting VMs (EC2)
* Storing data on virtual drives (EBS)
* Distributing load (ELB)
* Scaling services (ASG)

**EC2 VMs** has 5 distinct characteristics:

* RAM (type, amount, generation)
* CPU (type, make, frequency, generation, number of cores)
* I/O (disk performance, EBS optimizations)
* Network (bandwith, latency)
* GPU

**Burstable VMs** have OK CPU performance and when they need to process something
unexpected (spike in load) they can burst to VERY GOOD CPU performance.
When VM bursts it utilizes "burst credits" which when depleted make the CPU
performance BAD. These credits are accumulated over time when the VM is not
bursting. You can set the burstable VMs to be with "unlimited" so when the 
credits are depleted the performance won't suffer but extra cost is paid.

**Security Groups** are firewall for resources (i.e. EC2 instances).  
All inbound traffic is blocked by default.  
Connection Time Out issues point to Traffic not whitelisted on Security Group level.

**EC2 pricing** is on the second (billed every 60 seconds) and depends on region, 
instance type, whether the instance is on-demand, spot, reserved or dedicated host, 
what kind of OS is running and also storage used, data transferred and use of EIP.

**Scheduled Reserved Instances** are designed for workloads that recur on a daily, 
weekly, or monthly schedule, and are purchased for a one-year term.

**AMIs** are built for specific region. Custom AMIs are great for faster boot time
of custom software (i.e. in AutoScaling).

## ELB & ASG

### Scalability

Application/System can handle increased load by adapting 

* Vertical Scalability - Increase the size of the instance(s). Used in non-distributed 
  systems like databases.
* Horizontal Scalability (a.k.a Elasticity) - increase the number of instances. 
  Used in distributed systems like web applications.
  
### High-Availability 

Goes hand in hand with Horizontal Scalability.
HA means to run an application across at least 2 data centers (2 AZs)
with the goal to survive data center loss.

Can be:

* Passive - i.e RDS Multi-AZ
* Active - Horizontal scaling

### ELB Overview

Elastic Load Balancers:

* Forward traffic to multiple instances (in order to handle the load and provide redundancy
in case of an outage)
* Present single point of access (DNS record)
* Can perform Health Checks to their backend (to route traffic only to healthy instances) -
  Health Checks need a port (i.e. 80), a route (/health) and accepted response code (i.e. 200)
* Provide SSL termination
* Enforce Cookie Stickiness
* Ensure HA (at least 2 AZs needed to deploy)
* Separate public traffic (SRC -> LB) from private traffic (LB -> backend)

### ELB Types

* CLB - HTTP, HTTPS & TCP traffic (layer 4 & 7).
* ALB - HTTP, HTTPS & WebSocket traffic (layer 7).  
  * Can load balance between:
    * multiple HTTP apps across machines (via target groups)
    * multiple apps on the same machine (i.e. containers)  
  * Support HTTP/2 and WebSocket
  * Support redirects (i.e. from HTTP to HTTPS)
  * Support routing based on hostname, route or Query string 
  * Support port mapping to redirect to dynamic port on ECS - great for microservices
  * Client IP is not directly visible on the backend unless you use the X-Forwarded-For header  
  * Target groups could be made of:
    * EC2 (managed by ASG)
    * ECS tasks (managed by ECS)
    * Lambda functions (HTTP request is translated into JSON event)
    * IP addresses (must be private IP)
* NLB - TCP, TLS (secure TCP) & UDP (layer 4)
  * No security group (like with ALB)
  * Lower latency than ALB (~100ms vs ~400ms)
  * Can handle millions of requests per second (so used for extreme performance)
  * Support 1 static IP per AZ (and supports assigning EIP)

**NB!**
* LBs can scale but not instantly (contact AWS for warm-up)
* Troubleshooting:
  * 4xx are client induced errors
  * 5xx are app induced errors
  * 503 error meeans capacity limit reached or no registered target
  * If LB cannot connect to EC2 - check the LB SG and the EC2 SG
* Monitoring:
  * ELBs access logs will log each access request (you can debug per request)
  * CW Metrics can provide aggregated statistics (i.e. connections count)

### Stickiness

Applicable for CLB and ALB.  
Use case: You don't want the user to lose the session data.  
Means that the same client will always be redirected by the ELB to the same  
instance.  
The "cookie" used for stickiness has an expiration date that can be controlled.  
Enabling stickiness can cause imbalance to the backend load!  
On ALB stickiness is configured on target group level.  

### Cross-Zone Load Balancing

When enabled the LB distributes evenly across registered instances in all AZs 
(instead only within one AZ for each LB instance (IP)).  
CLBs have this disabled by default but on ALBs it's enabled and cannot be disabled.

### SSL Certificates

ELBs support x.509 SSL/TLS certificates via HTTPS listener for which:
* You should specify default certificate
* You can have multiple listeners with multiple certs to support multiple domains 
(ALBs & NLBs only as CLBs support only one SSL cert since it does not support SNI)  
* You can specify older security policy to support older versions of SSL/TLS (for legacy clients) 
* Clients can use SNI (Server Name Indication) to specify the hostname they reach

SNI:
* Solves the problem of loading multiple SSL certs on the same server 
in order to serve multiple websites.  
* Is newer protocol that requires the client to indicate the hostname 
of the target server in the initial SSL handshake. The server will then
find the correct certificate or return the default one.
* Only works for ALB and newer generation NLBs & CloudFront  
  
### Connection Draining

Time to complete "in-flight" requests while the instance is de-registering or unhealthy.    
Time range could be between 1 and 3600 seconds (default is 300). Can be disabled if set to 0.

It's called:
* "Connection Draining" in CLBs
* "Deregistration Delay" in Target Groups (ALBs & NLBs)

### ASG Overview

The goal is to automatically add/remove instances to match increasing/decreasing load
and make sure that min/max number of machines are running. Also can automatically register
new instances to an ELB.

ASG Attributes:
* Launch Configuration/Template - to define AMI, Instance Type, User Data, SGs, etc.
* Min/Max/Desired Size
* Network + Subnets info
* LB info
* Scaling Policies
  
**NB!**
* Scale Out -> Launch; Scale In -> Terminate;
* To update the ASG's instance config, provide new LC or new LT version.
* IAM roles attached to ASG (via LC or LT) will get assigned to EC2 instances in it.
* ASGs are free (you pay only for the resources launched by them).
* If instance in ASG is terminated the ASG will launch a replacement to meet size 
  configuration (ASGs terminate instances marked as unhealthy by ELB health checks)

### ASG Scaling Policies:

Overview:
* Scale ASG based on CW alarms (by monitoring metric such as CPU usage on ASG level).
* Scaling could be based on custom metric (i.e. number of users):
  * Put custom metric from instance to CW (PutMetricAPI)
  * Setup CW alarm to react to that metric low and high values
  * Use that CW alarm as scaling policy for the ASG
* Scaling rules could be:
  * Target average CPU usage
  * Number of ELB requests peer instance
  * Average Network In/Out
  * Scheduled for a specific timeframe (to meet known peaks)

Types:
* Target Tracking Scaling
  * Most simple and easy to do (i.e I want the avg. ASG CPU usage to stay ~40%)
* Simple / Step Scaling
  * When a CW alarm is triggered (i.e. if ASG CPU > 70% add 2 units and if CPU < 30% remove 1 unit)
* Scheduled Actions
  * Anticipate scaling based on known usage patterns (i.e. increase min size to 10 on 9am each day)
  
Scaling Cooldowns:
* Ensure ASGs do not launch/terminate additional instances before 
previous scaling activity takes effect.
* In addition to the default cooldown you can create cooldowns that apply to specific 
**simple scaling policy**. Such scaling-specific cooldowns override the default one. 

**NB!**

scaling action happens -> cooldown period in effect ? (yes) ignore action : (no) perform action

Scaling-specific cooldowns are typically used for scale-in policies.  
If your app is scaling up and down multiple times each hour, modify the ASG's
cooldown timers and the CW alarm period that triggers the scale-in.
  
## EBS & EFS

### EBS Overview

* Network drive for persistent data 
* Can be detached/attached on running instances
* Locked to an AZ (to move it across you need to snapshot it first and
  then restore the snapshot in another AZ)
* Has a provisioned capacity (size in GB and IOPS)
* Billing is for the provisioned capacity not the actually used one

### EBS Volume Types

Types:
* General purpose SSD (`gp2`) - Balanced price/performance for wide variety of workloads
  * volumes lower than 1000GB can burst IOPS up to 3000
  * 3 IOPS per GB => max IOPS of 16000 is reached on 5333GB
* General purpose SSD (`gp3`) - Like `gp2` but throughput can be scaled independently of the size 
  * Min IOPS is 3000 
* Provisioned IOPS SSD (`io1`) - Highest performance with low latency and high throughput for 
  mission-critical workloads (i.e. Critical DB)
  * maximum IOPS per GB ratio is 50:1
  * use if you need more than 16000 IOPS per volume
  * max IOPS for non Nitro instances is 32000 instead of 64000
* Provisioned IOPS SSD (`io2`) - Higher durability and more IOPS than `io1`  
* Throughput Optimized HDD (`st1`) - Low cost HDD for frequently accessed, throughput intensive
  workloads (i.e. Big Data)
* Cold HDD (`sc1`) - Lowest cost HDD for less frequently accessed workloads 

**NB!**
* Root EBS volumes get terminated by default on instance termination (can be disabled) 
* Only the SSD (`gp` and `io`) types can be used for boot volumes
* HDD type volumes are suitable for large, sequential I/O operations 

|                | gp2 (gp3)               | io1 (io2)     | st1            | sc1            |  
|----------------|-------------------------|---------------|----------------|----------------|
| Size           | 1GiB - 16TiB            | 4GiB - 16TiB  | 125GiB - 16TiB | 125GiB - 16TiB |
| IOPS           | 100 (3000) - 16000      | 100 - 64000   | n/a            | n/a            |
| Max Throughput | n/a                     | n/a           | 500 MiB/s      | 250 MiB/s      |

### Instance Store

* Used instead of EBS volumes (still block storage)
* Ephemeral storage (data does not persist through instance stop/start and no resize)
* Physically attached to the machine (Better I/O Performance but risk of data loss due to HW failure)
* Good for buffer / cache / temp. content

### EFS Overview

* Multi AZ and Petabyte-scale
* ~3x the EBS price  
* NFSv4.1 (only works with Linux)
* SG is used to control access
* Build for thousands of concurrent NFS clients (10GB+/s throughout)
* Performance modes
  * General purpose (default) - latency sensitive use cases (web server, CMS, etc.)
  * Max I/O - higher latency, throughput, highly parallel (big data, media processing)
* Throughput modes
  * Bursting - scales with FS size
  * Provisioned - fixed at specific amount
* Storage Tiers (lifecycle management)
  * Standard - frequently accessed files
  * Infrequent Access (EFS-IA) - cost to retrieve files, lower price to store
  
## RDS, Aurora & ElastiCache

### RDS Overview

AWS managed SQL-based relational DB that supports:
* Postgres
* MySQL
* MariaDB
* Oracle
* MS SQL Server
* Aurora (AWS proprietary DB)

Key properties:
* Read Replicas for improved READ performance
* Automated backups (for point-in-time recovery):
  * Full daily backups (from 1 up to 35 days retention)
  * Transaction logs backups every 5 min => able to restore from the oldest backup until 5 min ago
* Automated provisioning * OS patching
* Multi-AZ setup for DR 
* Scaling capability (Horizontal and Vertical)
* Can use `gp2` and `io1` storage

### RDS Read Replicas & Multi-AZ

Read Replicas:
* Can scale up to 5
* Can only use SELECT on them (not INSERT, UPDATE or DELETE)  
* Can be within AZ (for latency & cost) or cross AZ/region (for DR)
* Replication is ASYNC => eventual consistency
* Can be promoted to stand-alone DBs
* Connection string must be updated on app level if 

Multi-AZ:
* Replication is SYNC => strict consistency
* One DNS name => automatic app failover to standby DB in different AZ

**NB!**   
In AWS there is a network cost where data moves between AZs so if read replicas are not 
in the same AZ as the writer they wil induce extra cost for the replication traffic.

### RDS Security & Encryption

Encryption at rest:
* Can set up AES-256 KMS key during launch
* If master is not encrypted read replicas also cannot be
* Transparent Data Encryption (TDE) is available for Oracle and MS SQL as additional
  way to encrypt your DB (on top or instead of KMS)

Encryption in transit:
* SSL certs can encrypt traffic to RDS in flight
* SSL can be enforced as follows:
  * Postgres - set `rds.force_ssl = 1` in the RDS' parameter group
  * MySQL - execute `GRANT USAGE ON *.* TO '<user>'@'%' REQUIRE SSL;` within the DB
  
How to encrypt unencrypted RDS:
* Make an RDS snapshot (will be unencrypted as well)
* Copy the snapshot to another one and set it to be encrypted
* Restore the DB from the copied encrypted snapshot

Security:
* Network - SGs (who can reach the RDS)
* Management - IAM policies (who can manage the RDS)
* Authorization:
  * simple username/password 
  * IAM authentication (only for MySQL and Postgres) - uses authentication token for login
  that expires in 15 min and is obtained via API call

### Aurora Overview

Main properties:
* Supports MySQL and Postgres
* Storage grows automatically (with 10GB increments) up to 64TB 
* Can support up to 15 read replicas (compared to max 5 for simple RDS) and the
replication is faster (replica lag <= ~10ms)
* Fail over is instantaneous (as it's HA native)  
* Costs ~20% more than RDS 
* Backtrack - Can restore data at any points of time without using backups

HA and Read scaling:
* Stores 6 copies of your data across 3 AZs and needs:
  * 4 copies available for writes (1 AZ failure tolerance)
  * 3 copies available for reads 
* Self-healing with peer-to-peer replication
* Storage is striped across 100s of volumes
* Read replicas can auto scale

Aurora Serverless:
* Auto scale based on usage
* Good for infrequent or unpredictable workloads

Global Aurora:
* 1 Primary region for reads/writes
* Up to 5 secondary read-only regions (with replica lag < 1 sec)
* Up to 16 read replicas per secondary region
* Secondary region can be promoted to primary (in case of DR event) with RTO < 1 min

**NB!**  
You can have Aurora MySQL with multiple writers

### ElastiCache Overview

AWS Managed in-memory DB for high-performance and low latency that supports:
* Redis - can be used as database, cache and message broker
  * Read scaling using read replicas
  * Multi-AZ/Fail over capability
  * Backup/Restore features
  * Data durability (cache persistence after stop/start)
* Memcached - can be used as pure cache
  * Write scaling using sharding (multi-node for data partitioning)
  * Cache is non-persistent
  * No backup/restore features
  * Multi-threaded

How it works:
* Apps query ElastiCache and if not available get from RDS and store in ElastiCache
* Used to take load off DBs for read intensive workloads or share state by caching data.
* Helps making your app stateless (i.e. used for user session data)

### ElastiCache Strategies

* Lazy Loading / Cache-Aside / Lazy Population - Optimizes read experience. App queries cache and if
  not there it will get it from the DB and write it to the cache for subsequent use.
  * Pros:
    * Only requested data is cached
    * Node failures are not fatal (just increased latency get infro from DB again and store in cache)
  * Cons:
    * Read penalty = noticeable delay (to obtain the data from the RDS and store it in the cache if missed)
    * Stale data (updated in the RDS but not in the cache)
* Write Through - Typically used along with Lazy Loading to avoid stale data. 
  Apps add/update cache as well when DB is updated
  * Pros:
    * Data in cache is never stale and reads are quick
  * Cons:
    * Write penalty = some delay when writing data (due to 2 write calls instead of 1)
    * Cache churn = a lot of the data stored in cache will never be read

Cache Evictions and TTL:
* Cache eviction can occur in 3 ways:
  * delete the item explicitly
  * item is deleted due to full memory and not being recently used (LRU)
  * TTL for the item is set
* TTL can range from seconds to days.
* It's bad idea to use TTL with Write Through  
* If cache evictions happen a lot due to memory => scale up or out

## Route53

## VPC

## S3
